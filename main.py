import sys


def S():
    c = getchar()                   #в этой грамматике все строки начинаются с "a"
    if c != "a":
        error()
    S_dash()
    c = getchar()
    if c != '$':                    # $ - символ конца строки
        error()


def S_dash():
    c = getchar()
    if c == "b":
        A()
    elif c == "c":
        B()
        A()
    elif c == "a":
        C()
    else:
        error()


def A():
    c = getchar()
    if c == "b":                    #если мы встретили нетерминал "А" - сначала точно идет терминал "а"
        Z()
    else:
        error()


def A_dash():
    c = getchar()
    if c == "b":
        Z()
        c = getchar()
        if c == "a":
            Z()
        else:
            error()
    elif c == 'a':
        Z()
    elif c == 'b':
        Z()
    else:
        error()


def Z():
    global pointer
    c = getchar()
    if c is not None:
        A_dash()
    elif c is None:
        pointer -= 1
    else:
        error()


def B():
    c = getchar()
    if c == 'b':
        B()
    else:
        error()


def B_dash():
    c = getchar()
    if c != "b":
        error()
    B()


def C():
    c = getchar()
    if c == "c":
        Y()
    else:
        error()


def C_dash():
    c = getchar()
    if c == "c":
        Y()
    else:
        error()


def Y():
    global pointer
    c = getchar()
    if c is not None:
        C_dash()
    elif c is None:
        pointer -= 1
    else:
        error()


def getchar():
    global pointer
    global str1
    c = str1[pointer]
    pointer += 1
    return c


def error():
    print("ERROR OCCURRED\nstr: ", str1, "\npointer value: ", pointer)
    sys.exit()


if __name__ == "__main__":
    global pointer
    global str1
    print("Enter a line like abaa$, where $ - EOF symbol")
    str1 = input()
    pointer = 0
    S()
    print("Success!")